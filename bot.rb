require 'discordrb'

class BotBoobsFinder < Discordrb::Commands::CommandBot
    def initialize(token, prefix, parser)
        super(token: token, prefix: prefix)
        puts "Invite URL is #{invite_url}"
        
        ready do |event|
            update_status('online', prefix+'help', nil)
        end

        command :help do |event|
            event << prefix + 'help: This help message !'
            event << prefix + "show: I will give you a nice looking picture :)"
            event << prefix + 'invite: Get the invite URL for this bot to use on your own server !'
            event << prefix + 'update: Update the database of the pictures'

        end
        command :invite do |event|
            event << "You can invite me with #{invite_url}"
        end

        command :show do |event|
            img = parser.getNextImage(event.server.id)

            msg = event.send_embed do |embed|
                embed.title = ":frame_photo: #{img[:title]}"
                embed.color = 11683347

                embed.image = Discordrb::Webhooks::EmbedImage.new(url: img[:src])

                embed.url = img[:url]

                embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "from /r/#{img[:origin]}, requested by #{event.user.username}")

            end
        end

        command :update do |event|
            if(parser.check_parse)
                event << "Bot is updated with more image for your pleasure !"
            else
                event << "Bot is already up to date."
            end
        end

        run
    end
    
end