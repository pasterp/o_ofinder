require 'yaml'
require 'json'

require './bot.rb'
require './parser.rb'

config = JSON.parse(YAML.load(File.read("config.yml")).to_json, object_class: OpenStruct)

parser = ScheduledParser.new config

trap "SIGINT" do
    parser.save
    exit 130
end


bot = BotBoobsFinder.new config.Discord.bot_token, config.Discord.prefix, parser
