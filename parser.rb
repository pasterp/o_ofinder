require 'redd'
require 'thread'
require 'time'
require 'yaml'

class ScheduledParser 
    def initialize(config)
        @session = Redd.it(
            user_agent: 'Redd:OOFinder:v0.1',
            client_id: config.Reddit.client_id,
            secret: config.Reddit.secret,
            username: config.Reddit.username,
            password: config.Reddit.password
        )
        @subs = config.Reddit.subreddits

        @boobies = Array.new
        @next_parse = Time.now + (60 * 60 * 24)
        @servers = {} #{id => [random,inc]}

        load()
        save()

        check_parse()
    end
    def check_parse
        if(Time.now > @next_parse ||@boobies.empty?)
            parseAll()
            return true
        else
            return false
        end
    end

    def parseAll()
        threads = []
        @subs.each do |sub|
            threads << Thread.new(sub) do |s|
                @session.subreddit(s).listing(:top, {:time => :day } ).each do |post|
                    if(!post.is_video && !(post.url  =~ /gif/))
                        @boobies << {src:post.url, url:'https://reddit.com'+post.permalink, title:post.title, origin:s}
                    end        
                end
            end
        end
        threads.each { |thr| thr.join }
        save(true)
        puts "New images have been parsed !"
        @next_parse = Time.now + (60 * 60 * 24)
    end

    def load
        begin
            @boobies = YAML.load(File.read("data.yml"))
        rescue => exception
            puts "No data to load."
        end

        begin
            meta = YAML.load(File.read("meta.yml"))

            @servers = meta[:servers]
            @next_parse = meta[:next_parse]
            puts "Configuration loaded successfully"

        rescue => exception
            puts "No configuration to load."
        end
    end

    def save(save_images = false)
        if(save_images)
            File.open("data.yml", "w") { |file| file.write(@boobies.to_yaml) }
        end
        File.open("meta.yml", "w") { |file| file.write({:servers => @servers, :next_parse => @next_parse}.to_yaml) }
    end

    def getNextImage(server_id)
        return @boobies[getServerRandom(server_id) % @boobies.size]
    end
    def getServerRandom(server_id)
        if(!@servers[server_id])
            @servers[server_id] = [Random.new_seed,0]
        end

        srand (@servers[server_id][0] + @servers[server_id][1])
        @servers[server_id][1]+=1
    end
end